
## For Windows:
1. Download Unity 2018.1.0 (newer version does not support ARCore) and install it.  

2. Follow this tutorial for creating HelloAR app for Android: https://developers.google.com/ar/develop/unity/quickstart-android  

3. If you get an error saying that Android SDK is not found, then take these steps:  

    i. Delete android sdk "tools" folder : [Your Android SDK root]/tools -> tools  
    ii. Download SDK Tools: http://dl-ssl.google.com/android/repository/tools_r25.2.5-windows.zip  
    iii. Extract that to Android SDK root  
    iv. Build your project
